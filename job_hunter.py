import json
from datetime import datetime
from elasticsearch import Elasticsearch
from typing import NamedTuple, Dict
import hashlib
from geopy.geocoders import Nominatim


def get_offers_from_wizbii():
    import requests

    url = "https://ug9ksbmo1v-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser&x-algolia-application-id=UG9KSBMO1V&x-algolia-api-key=YmIyYmUyN2FhNzQ4ZTMxMjk4NjZmYmQwZGYxNzQyOWFhNjM1MWY1NTljYmNhYzliY2ZjYzEzN2U5ZTk2NjI2MnJlc3RyaWN0SW5kaWNlcz0lNUIlMjJwcm9kX2NvbXBhbmllcyUyMiUyQyslMjJwcm9kX2pvYnMlMjIlMkMrJTIycHJvZF9zY2hvb2xzJTIyJTJDKyUyMnByb2RfY29udGVudF9hcnRpY2xlXyUyQSUyMiU1RA%3D%3D"

    payload = '{\r\n    "requests": [\r\n        {\r\n            "indexName": "prod_jobs",\r\n            "params": "query=data engineer"\r\n        }\r\n    ]\r\n}'
    headers = {"Content-Type": "text/plain"}

    response_wizbii = requests.request("POST", url, headers=headers, data=payload)

    # print(response_wizbii.text.encode('utf8'))
    return response_wizbii.json()


def get_offers_from_smart_recruiters():
    import requests

    url = "https://jobs.smartrecruiters.com/sr-jobs/search?keyword=data+scientist&limit=100"

    payload = {}
    headers = {
        "Cookie": "__cfduid=d6b46ebc4e3a44b976086f133f3b4e6ab1590063614; AWSALB=U4f9CsQ+oAYLK3YMYBseCsdVVo4hueRjzK6oIjeI9JaoLxnnv53XVaaOtiGRtk6EPL6ITrHNP3v1H+ISdBrZpLcIC4kQRct5b7XdNBwLAIe0X3vdWbPsv0wrpZdp; AWSALBCORS=U4f9CsQ+oAYLK3YMYBseCsdVVo4hueRjzK6oIjeI9JaoLxnnv53XVaaOtiGRtk6EPL6ITrHNP3v1H+ISdBrZpLcIC4kQRct5b7XdNBwLAIe0X3vdWbPsv0wrpZdp"
    }

    response_smart_recruiters = requests.request("GET", url, headers=headers, data=payload)

    # print(response_smart_recruiters.text.encode('utf8'))
    return response_smart_recruiters.json()


def get_offers_from_wttj():
    import requests

    url = "https://csekhvms53-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.24.12%3Breact-instantsearch%205.3.2%3BJS%20Helper%20(2.28.0)&x-algolia-application-id=CSEKHVMS53&x-algolia-api-key=02f0d440abc99cae37e126886438b266"

    payload = '{\n    "requests": [\n        {\n            "indexName": "wk_cms_jobs_production",\n            "params": "query=data%20engineer&hitsPerPage=30&maxValuesPerFacet=10&page=0&restrictSearchableAttributes=%5B%22name%22%2C%22profession.category%22%2C%22profession.name%22%2C%22profile%22%2C%22description%22%2C%22organization.name%22%2C%22office.city%22%2C%22office.district%22%2C%22office.state%22%2C%22office.country%22%2C%22department%22%2C%22contract_type_names.fr%22%2C%22sectors.name.fr%22%2C%22sectors.parent.fr%22%5D&highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&filters=website.reference%3Awttj_fr&facets=%5B%22office.country_code%22%2C%22office.state%22%2C%22office.district%22%2C%22office.location%22%2C%22contract_type_names.fr%22%2C%22experience_level_minimum%22%2C%22remote%22%2C%22organization.size.fr%22%2C%22language%22%5D&tagFilters=&facetFilters=%5B%22office.country_code%3AFR%22%5D&numericFilters=%5B%22experience_level_minimum%3E%3D0%22%2C%22experience_level_minimum%3C%3D15%22%5D"\n        },\n        {\n            "indexName": "wk_cms_jobs_production",\n            "params": "query=data%20engineer&hitsPerPage=1&maxValuesPerFacet=10&page=0&restrictSearchableAttributes=%5B%22name%22%2C%22profession.category%22%2C%22profession.name%22%2C%22profile%22%2C%22description%22%2C%22organization.name%22%2C%22office.city%22%2C%22office.district%22%2C%22office.state%22%2C%22office.country%22%2C%22department%22%2C%22contract_type_names.fr%22%2C%22sectors.name.fr%22%2C%22sectors.parent.fr%22%5D&highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&filters=website.reference%3Awttj_fr&attributesToRetrieve=%5B%5D&attributesToHighlight=%5B%5D&attributesToSnippet=%5B%5D&tagFilters=&analytics=false&clickAnalytics=false&facets=experience_level_minimum&facetFilters=%5B%22office.country_code%3AFR%22%5D"\n        }\n    ]\n}'
    headers = {
        "Referer": "https://www.welcometothejungle.com/fr/jobs?query=data%20engineer&page=1",
        "Content-Type": "application/json",
    }

    response_wttj = requests.request("POST", url, headers=headers, data=payload)

    # print(response_wttj.text.encode('utf8'))
    return response_wttj.json()


def get_coordinate(city_name: str) -> Dict:
    geolocator = Nominatim(user_agent="job_offer")
    location = geolocator.geocode(city_name)
    return {"lat": location.latitude, "long": location.longitude}


class JobOffer(NamedTuple):
    id: str
    title: str
    url: str
    logo: str
    company: str
    location: str
    contract_type: str
    publication_date: datetime
    resume: str
    plateform: str
    geolocation: Dict

    def to_dict(self):
        return self._asdict()


if __name__ == "__main__":

    job_offers_wttj = get_offers_from_wttj()["results"]
    list_offers = []
    es_client = Elasticsearch(hosts=[{"host": "34.244.39.111", "port": 9200}])
    for offer in job_offers_wttj[0]["hits"]:
        # print(offer)

        title = offer["name"]
        url = offer["organization"]["website_organization"]["slug"]
        logo = offer["organization"]["logo"]["url"]
        company = offer["organization"]["name"]
        location = offer["office"]["city"]
        contract_type = offer["contract_type"]
        publication_date = offer["published_at"]
        resume = offer["profile"]
        plateform = offer["website"]["reference"]
        geolocation = offer["_geoloc"]
        id = hashlib.md5(
            title.encode("utf-8") + company.encode("utf-8") + str(publication_date).encode("utf-8")
        ).hexdigest()

        offer_wttj = JobOffer(
            id, title, url, logo, company, location, contract_type, publication_date, resume, plateform, geolocation
        )
        list_offers.append(offer_wttj)

    job_offers_smart_recruiters = get_offers_from_smart_recruiters()
    for offer in job_offers_smart_recruiters["content"]:
        # print(offer)
        title = offer["name"]
        url = offer["applyUrl"]
        logo = offer["company"]["logo"]
        company = offer["company"]["name"]
        location = offer["location"]["city"]
        contract_type = ""
        publication_date = offer["releasedDate"]
        resume = ""
        plateform = "https://www.smartrecruiters.com/"
        geolocation = get_coordinate(location)
        id = hashlib.md5(
            title.encode("utf-8") + company.encode("utf-8") + str(publication_date).encode("utf-8")
        ).hexdigest()

        offer_smart_recruiters = JobOffer(
            id, title, url, logo, company, location, contract_type, publication_date, resume, plateform, geolocation
        )
        list_offers.append(offer_smart_recruiters)

    job_offers_wizbii = get_offers_from_wizbii()["results"]
    for offer in job_offers_wizbii[0]["hits"]:
        # print(offer)
        title = offer["title"]
        url = offer["externalUrl"]
        logo = offer["company"]["logo"]
        company = offer["company"]["name"]
        location = offer["location"]["city"]
        try:
            contract_type = offer["contract"]["titleShort"]
        except Exception as e:
            contract_type = "unknown"
        publication_date = offer["@timestamp"]
        resume = offer["profile"]
        plateform = "https://www.wizbii.com/"
        geolocation = offer["_geoloc"]
        id = hashlib.md5(
            title.encode("utf-8") + company.encode("utf-8") + str(publication_date).encode("utf-8")
        ).hexdigest()

        offer_wizbii = JobOffer(
            id, title, url, logo, company, location, contract_type, publication_date, resume, plateform, geolocation
        )
        list_offers.append(offer_wizbii)
    # print(list_offers)
    for offer in list_offers:
        try:
            es_client.create(index="job_offers", body=json.dumps(offer.to_dict()), id=offer.id)
        except Exception as e:
            print(e)
            print(offer)
            print(type(offer.geolocation))
