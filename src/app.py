from elasticsearch import Elasticsearch
from flask import Flask

app = Flask(__name__)
from flask import request


@app.route("/get_job_offers", methods=["GET"])
def get_job_offers():
    title = request.args.get("title")
    location = request.args.get("location")
    contract_type = request.args.get("contract_type")
    print(title, location, contract_type)
    res = get_offers_from_es(title, location, contract_type)
    return res


def get_offers_from_es(title, location, contract_type):
    elasticsearch: Elasticsearch = Elasticsearch(hosts=[{"host": "34.244.39.111", "port": 9200}])
    res = elasticsearch.search(
        index="job_offers",
        body={
            "query": {
                "bool": {
                    "must": [
                        {"match": {"title": title}},
                        {"match": {"location": location}},
                        {"match": {"contract_type": contract_type}},
                    ]
                }
            }
        },
    )
    return res
